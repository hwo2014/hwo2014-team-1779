package noobbot;

import noobbot.model.Join;
import noobbot.model.in.CarPositions;
import noobbot.model.in.YourCar;

import com.google.gson.Gson;

public class ModelTest {
    
    // I dont include Junit now
    
    public static void main(String[] args) {
        ModelTest test = new ModelTest();
        Gson gson = new Gson();
        
        test.testYourCar(gson);
        test.testJoin(gson);
        test.testCarPos(gson);
        
        System.out.println("OK");
    }
    
    public void testYourCar(Gson gson) {
        String json = "{\"msgType\": \"yourCar\", \"data\": {\"name\": \"Senna\",\"color\": \"red\" }}";
        YourCar car = gson.fromJson(json, YourCar.class);
        if (!"red".equals(car.getColor()) || !"Senna".equals(car.getName()) || !"yourCar".equals(car.msgType())) {
            throw new AssertionError();
        }
    }
    
    public void testJoin(Gson gson) {
      /*  String json = "{\"msgType\": \"join\", \"data\": { \"name\": \"Schumacher\", \"key\": \"UEWJBVNHDS\"}}";
        Join join = gson.fromJson(json, Join.class);
        if (!"UEWJBVNHDS".equals(join.getKey()) || !"Schumacher".equals(join.getName()) || !"join".equals(join.msgType())) {
            throw new AssertionError();
        }*/
    }
    
    public void testCarPos(Gson gson) {
        String json = "{\"msgType\":\"carPositions\", \"data\":[{ \"id\":{\"name\":\"Senna\",\"color\":\"red\"},"
                + "\"angle\":0.0, \"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":5.212619150159847,"
                + "\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0}, \"lap\":1}}],"
                + "\"gameId\":\"6648cdd0-09f8-49ec-8e01-58d5ebd1d5c7\", \"gameTick\":750}";
        CarPositions pos = gson.fromJson(json, CarPositions.class);
        if (!"Senna".equals(pos.data[0].getId().getName())) {
            throw new AssertionError();
        }

    }

}
