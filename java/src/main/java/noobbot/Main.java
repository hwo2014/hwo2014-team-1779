package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.model.Join;
import noobbot.model.MsgWrapper;
import noobbot.model.Ping;
import noobbot.model.SendMsg;
import noobbot.model.Throttle;
import noobbot.model.in.CarPositions;
import noobbot.model.in.YourCar;

import com.google.gson.Gson;

public class Main {
    
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println(String.format("Connecting to '%s:%d' as %s/%s", host, port, botName, botKey));

        try (final Socket socket = new Socket(host, port);
             final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
             final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));) {

             new Main(reader, writer, new Join(botName, botKey));
        }
    }

    private final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

            switch (msgFromServer.msgType) {
            case "carPositions":
                CarPositions pos = gson.fromJson(line, CarPositions.class);
                send(new Throttle(0.632));
                break;
            case "yourCar":
                YourCar car = gson.fromJson(line, YourCar.class);
                break;
            case "join":
                System.out.println("Joined");
                break;
            case "gameInit":
                System.out.println("Race init");
                break;
            case "gameEnd":
                System.out.println("Race end");
                break;
            case "gameStart":
                System.out.println("Race start");
                break;
            default:
                send(new Ping());
                break;
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}