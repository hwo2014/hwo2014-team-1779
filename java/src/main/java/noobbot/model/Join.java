package noobbot.model;

public class Join extends SendMsg {
    public String name;
    public String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String msgType() {
        return "join";
    }
    
}