package noobbot.model.in;

public class Lane {
    private int startLaneIndex;
    private int endLaneIndex;
    
    public int getStartLaneIndex() {
        return startLaneIndex;
    }
    
    public int getEndLaneIndex() {
        return endLaneIndex;
    }

}
