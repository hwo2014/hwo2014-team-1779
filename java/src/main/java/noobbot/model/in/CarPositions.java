package noobbot.model.in;

import noobbot.model.SendMsg;

public class CarPositions extends SendMsg {
    public String msgType;
    public Data[] data;
    // gameId
    public int gameTick;

    @Override
    protected String msgType() {
        return msgType;
    }

    public Data[] getData() {
        return data;
    }

    public class Data {
        private CarID id;
        private double angle;
        private PiecePosition piecePosition;
        
        public CarID getId() {
            return id;
        }
        public double getAngle() {
            return angle;
        }
        public PiecePosition getPiecePosition() {
            return piecePosition;
        }

    }

}

