package noobbot.model.in;

public class CarID {
    private String name;
    private String color;

    CarID(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

}
