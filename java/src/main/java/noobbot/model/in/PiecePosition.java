package noobbot.model.in;

public class PiecePosition {
    private int pieceIndex;
    private double inPieceDistance;
    private Lane lane;
    private int lap;
    
    public int getPieceIndex() {
        return pieceIndex;
    }
    
    public double getInPieceDistance() {
        return inPieceDistance;
    }
    
    public Lane getLane() {
        return lane;
    }
    
    public int getLap() {
        return lap;
    }

}
