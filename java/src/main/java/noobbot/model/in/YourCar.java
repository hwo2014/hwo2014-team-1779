package noobbot.model.in;

import noobbot.model.SendMsg;

public class YourCar extends SendMsg {
    private CarID data;

    public CarID getData() {
        return data;
    }

    @Override
    public String msgType() {
        return "yourCar";
    }
    
    public String getName() {
        return data.getName();
    }
    
    public String getColor() {
        return data.getColor();
    }
    
}
