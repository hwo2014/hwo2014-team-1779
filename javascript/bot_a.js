var util = require('util');
var analyze = require('./analyzer.js');
module.exports = function(){

	var myColor = "";
	var trackPieces = [];
	var lanes = [];	
	var cars = [];
	var numberOfLaps = 0;

	var currentPos = { pieceIndex: 0, inPieceDistance: 0 };
	var currentAngle = 0;
	var currentSpeed = 0;
	var currentAcc = 0;
	var currentThrottle = 0.5;
	var currentLap = 0;

	function myCar(data){
		myColor = data.color;
		console.log('myColor:' + myColor );
	}

	function initRace(race){
		trackPieces = race.track.pieces;
		analyze(trackPieces);
		lanes = race.track.lanes;		
		cars = race.cars;
		numberOfLaps = race.raceSession.laps;
		console.log('initRace laps:' + numberOfLaps );
	}

	function update(positions){
		var pos = findMyCar(positions);
		var speed = getSpeed(currentPos, pos.piecePosition);
		currentAcc = speed - currentSpeed;
		currentSpeed = speed;
		currentPos = pos.piecePosition;
		currentAngle = pos.angle;
		currentLap = currentPos.lap;		

		updateTrackHistory();
		logCurrent();
	}

	function updateTrackHistory(){
		var index = currentPos.pieceIndex;
		if (!trackPieces[index].history){
			trackPieces[index].history = [];
		}
		if (trackPieces[index].history.length < currentLap  + 1){
			trackPieces[index].history.push({ angle: currentAngle, speed: currentSpeed });
		}else if (trackPieces[index].history[currentLap].angle < currentAngle){
			trackPieces[index].history[currentLap] = { angle: currentAngle, speed: currentSpeed };
		}		
	}

	function getNextTargetSpeed(){
		var target = null;
		var difficulty = trackPieces[currentPos.pieceIndex].cornerDifficulty;

		if (difficulty === 0){
			if (trackPieces.length > currentPos.pieceIndex + 1){
				difficulty = trackPieces[currentPos.pieceIndex + 1].cornerDifficulty;
			}else{
				difficulty = trackPieces[0].cornerDifficulty;
			}
		}
		console.log('difficulty:' + difficulty);
		if (difficulty > 0){
			target = findMaxSpeedForDifficulty(difficulty);
		}

		return target;
	}

	function findMaxSpeedForDifficulty(difficulty){		
		for(var i = 0;i<trackPieces.length;i++){
			if (trackPieces[i].cornerDifficulty - 0.2 < difficulty 
				&& trackPieces[i].cornerDifficulty + 0.2 > difficulty 
				&& trackPieces[i].history){

				var relevantLap = currentLap;
				if (trackPieces[i].history.length <= relevantLap){
					relevantLap = currentLap - 1;
				}
				var relevantHistory = trackPieces[i].history[relevantLap];
				if (relevantHistory.angle > 10.0 && relevantHistory.angle < 30.0){
					return relevantHistory.speed;
				}
			}
		}
		return null;
	}

	function getThrottle2(){
		var speed = getNextTargetSpeed();		
		console.log('targetSpeed:' + speed);
		if (speed === null){
			currentThrottle = 1.0;
		}else if (speed > currentSpeed){
			currentThrottle = 1.0;
		}else{
			currentThrottle = 0.0;
		}				

		return currentThrottle;
	}

	function getThrottle(){
		var nextPiece = currentPos.pieceIndex + 1;		
		if (nextPiece >= trackPieces.length) nextPiece = 0;

		var crashWith = trackPieces[currentPos.pieceIndex].crashWith || trackPieces[nextPiece].crashWith || -1.0;

		if (crashWith > 0){
			console.log('crash was with ' + crashWith);
			if (currentSpeed * 2.0 > crashWith){
				currentThrottle = currentThrottle / 10.0;
			}else{
				currentThrottle += 0.5;
			}
		}
		else if (!trackPieces[currentPos.pieceIndex].angle && !trackPieces[nextPiece].angle){
			currentThrottle = 1.0;	
		}	
		else if (Math.abs(currentAngle) > 40)	{
			trackPieces[currentPos.pieceIndex].crashWith = currentSpeed;
			currentThrottle = currentThrottle/8.0;		
		}else if(Math.abs(currentAngle) > 20){
			currentThrottle = currentThrottle/4.0;
		}
		else{
			currentThrottle = 0.5;
		}

		if (currentThrottle > 1.0) currentThrottle = 1.0;
		return currentThrottle;
	}

	function crash(data){
		if (data.color === myColor){
			trackPieces[currentPos.pieceIndex].crashWith = currentSpeed;
		}
	}

	function logCurrent(){
		console.log('throttle:' + currentThrottle + ' acc:' + currentAcc + ' speed:' + currentSpeed +  ' angle:' + currentAngle + ' lap:' + currentLap);
		console.log('pos:' + util.inspect(currentPos, false, null));
	}

	function findMyCar(positions){
		return positions.filter(function(car){
			return car.id.color === myColor;
		})[0];
	}

	function getSpeed(pos1, pos2){
		if (pos1.pieceIndex === pos2.pieceIndex){
			return pos2.inPieceDistance - pos1.inPieceDistance;
		}else{
			//TODO
			return currentSpeed;
		}
	}	

	return {
		myCar: myCar,
		initRace: initRace,
		update: update,
		getThrottle: getThrottle2,
		crash: crash
	};
};