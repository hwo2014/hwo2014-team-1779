"use strict";

var unit = 1.0 / 4500.0;

function setCornerDifficulty(piece){	
	piece.cornerDifficulty = Math.abs(piece.angle) * piece.radius * unit;	
}

module.exports = function(trackPieces){
	for(var i=0;i<trackPieces.length;i++){
		if (trackPieces[i].angle){
			setCornerDifficulty(trackPieces[i]);		
		}else{
			trackPieces[i].cornerDifficulty = 0;
		}
		if(trackPieces[i].cornerDifficulty > 0) console.log(trackPieces[i].cornerDifficulty);
	}
}