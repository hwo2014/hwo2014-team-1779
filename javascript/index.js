var net = require("net");
var JSONStream = require('JSONStream');

var bot_a = require('./bot_a.js');
var bot = new bot_a();

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    bot.update(data.data);
    send({
      msgType: "throttle",
      data: bot.getThrottle()
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if ( data.msgType === 'yourCar'){
      bot.myCar(data.data);
    } else if (data.msgType === 'gameInit'){
      bot.initRace(data.data.race);
    } else if (data.msgType === 'crash'){
      bot.crash(data);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
